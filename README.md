## `sizeof` -- Command-line `sizeof` utility for C types

**Usage:** `sizeof <typedef, struct, or union name>`, where the type is defined
somewhere in the current directory, or a subdirectory thereof.
    
Computes and outputs the `sizeof` an arbitrary type.

DOES query compiler for native primitive sizes (e.g. `int`);
DOES NOT process the codebase with any compiler.

DOES:
* Recursively compute field sizes as necessary
* Factor array types of arbitrary dimensions
* Evaluate macros and enum values used in array sizes
* Calculate alignment padding
* WARNING: Treats function pointers as generic pointers -- NOT guaranteed by the C spec!


See the Issues page for more about what it DOES NOT do. I wrote this script out of
personal need; I am open to ongoing improvements for other people's needs, but
right now, it's messy and may fail on edge-cases I don't yet know about!